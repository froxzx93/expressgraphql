export const tienda = async (id) => {
    try {
        const marketsCollection = db.collection('tiendas');
        let market = {};
        const get = await marketsCollection.doc(id).get()
            .then(doc => {
                if (doc.exists) {
                    market.nombre = doc.data().nombre,
                        market.categoria = doc.data().categoria,
                        market.id = doc.id,
                        market.direccion = doc.data().direccion
                }
            })
        return market;
    }
    catch (err) {
        return console.log("Error getting documents",err)
    }
}