export const addTienda = async (object) => {
    try {
        const marketsCollection = db.collection('tiendas').doc();
        object.id = marketsCollection.id;
        const addMarket = await marketsCollection.set({
            nombre: object.nombre,
            categoria: object.categoria,
            direccion: object.direccion
        });
        return object
    }
    catch (err) {
        return console.log("Error getting documents", err)
    }
}