export const deleteTienda = async (object) => {
    try {
        const message = {};
        const marketsCollection = db.collection('tiendas').doc(object.id);
        const deleteMarket = await marketsCollection
            .get()
            .then(doc => {
                if (doc.exists) {
                    message.text = "Delete successfully" + object.id;
                    message.status = 200;
                    marketsCollection.delete();
                } else {
                    message.text = "Document not exists " + object.id;
                    message.status = 500;
                }
            });
        return message;
    }
    catch (err) {
        return console.log("Error getting documents", err)
    }
}