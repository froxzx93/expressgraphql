export const tiendas = async () => {
    try {
        const tiendasCollection = db.collection('tiendas');
        const all_tiendas = [];
        const get = await tiendasCollection.get()
            .then(snapshot => {
                snapshot.forEach(doc => {
                    all_tiendas.push({
                        id: doc.id,
                        categoria: doc.data().categoria,
                        nombre: doc.data().nombre,
                        direccion: doc.data().direccion,
                    });
                })

            })
        return all_tiendas;
    }
    catch (err) {
        return console.log("Error getting documents",err)
    }
}