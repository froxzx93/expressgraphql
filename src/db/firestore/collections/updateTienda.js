export const updateTienda = async (object) => {
    try {
        const marketsCollection = db.collection('tiendas').doc(object.id);
        const updateMarket = await marketsCollection
            .get()
            .then(doc => {
                if (doc.exists) {
                    marketsCollection.set({
                        nombre: object.nombre,
                        categoria: object.categoria,
                        direccion: object.direccion,
                    })
                }
            })
        return object
    }
    catch (err) {
        return console.log("Error getting documents", err)
    }
}