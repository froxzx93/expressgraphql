import * as admin from 'firebase-admin';
import serviceAccount from './serviceAccountKey.json';
import { tiendas } from './collections/tiendas';
import { tienda } from './collections/tienda';
import { addTienda } from './collections/addTienda';
import { updateTienda } from './collections/updateTienda';
import { deleteTienda } from './collections/deleteTienda';

// Connect to firestored
export const connectFirestore = async () => {
    try {
        await admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: 'https://express-base.firebaseio.com'
        });
        console.log("DB Firestore is connnected")
        global.db = admin.firestore();
    }
    catch (err) {
        console.log("Error Firestore in connnected", err)
    }
}

export const markets = async () => {
    try {
        return await tiendas()
    }
    catch (e) {
        console.log("getting documents tiendas", e)
    }
};

export const market = async (id) => {
    try {
        return await tienda(id)
    }
    catch (e) {
        console.log("getting document", id, e)
    }
};

export const addMarket = async (market) => {
    try {
        return await addTienda(market)
    }
    catch (e) {
        console.log("getting document", id, e)
    }
};

export const updateMarket = async (market) => {
    try {
        return await updateTienda(market)
    }
    catch (e) {
        console.log("getting document", id, e)
    }
};

export const deleteMarket = async (id) => {
    try {
        return await deleteTienda(id)
    }
    catch (e) {
        console.log("getting document", id, e)
    }
};