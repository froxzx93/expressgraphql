import { makeExecutableSchema } from 'graphql-tools';
import { resolvers } from './resolvers';

const typeDefs = `
    type Query {
        getMarkets: [Market]
        getMarket(id: String!): Market 
    }

    type Mutation {
        pushMarket(input: addMarket):Market
        setMarket(input: updateMarket):Market
        deleteMarket(input:deleteMarket!):Message
    }

    type Market {
        id: String
        categoria : String
        nombre: String
        direccion: String
    }

    type Message {
        text: String
        status: Int
    }
    input addMarket {
        categoria : String
        nombre: String
        direccion: String
    }
    
    input updateMarket {
        id: String!
        categoria : String
        nombre: String
        direccion: String
    }

    input deleteMarket {
        id: String!
    }

`;

export default makeExecutableSchema({
    typeDefs: typeDefs,
    resolvers: resolvers
});