import { markets } from '../db/firestore/index';
import { market } from '../db/firestore/index';
import { addMarket } from '../db/firestore/index';
import { updateMarket } from '../db/firestore/index';
import { deleteMarket } from '../db/firestore/index';

export const resolvers = {
    
    Query: {
        getMarkets: async () => {
            return await markets();
        },
        getMarket: async (root, { id }) => {
            return await market(id);
        },
    },

    Mutation: {
        pushMarket: async (_, { input }) => {
            return await addMarket(input);
        },
        setMarket: async (_, { input }) => {
            return await updateMarket(input);
        },
        deleteMarket: async (_, { input })=>{
            return await deleteMarket(input);
        }
    }

};