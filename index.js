import express from 'express';
import { development } from './prop';
import express_graphql from 'express-graphql';
import schema from './src/schemeGraphql/typeDefs';
import { connectFirestore } from './src/db/firestore/index';

// Connect to firestore
connectFirestore();

const app = express();

app.use("/graphql", express_graphql({
	graphiql: true,
	schema: schema
}))

app.listen(development.port, () => {
	console.log(development.message, development.port)
});