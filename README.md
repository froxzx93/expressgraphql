## Dependence 

### Dependence Base
- Nodejs v8
- npm v6
### Dependence Prod
- Express
- Firebase admin
- Graphql
- Express graph
- Graphql tools
### Dependence Dev
- Nodemon
- Rimraf
- Babel cli
- Babel ES6

## User Manual
- Git clone
- npm install
- add file “firebaseKey.json” in /src/db/firestore/serviceAccountKey.json
- Create collections in firestore

    - ![](https://paper-attachments.dropbox.com/s_F4E2FA96254E86D594A3F0D2B28A7DDB152D7A0D8DB8EB9863AA383AC51FAE28_1563749945224_imagen.png)
    - ![](https://paper-attachments.dropbox.com/s_F4E2FA96254E86D594A3F0D2B28A7DDB152D7A0D8DB8EB9863AA383AC51FAE28_1563749960180_imagen.png)
    - ![](https://paper-attachments.dropbox.com/s_F4E2FA96254E86D594A3F0D2B28A7DDB152D7A0D8DB8EB9863AA383AC51FAE28_1563749971950_imagen.png)

- npm start

    - ![](https://paper-attachments.dropbox.com/s_F4E2FA96254E86D594A3F0D2B28A7DDB152D7A0D8DB8EB9863AA383AC51FAE28_1563750003917_imagen.png)

- View http://localhost:3000/graphql

    - ![](https://paper-attachments.dropbox.com/s_F4E2FA96254E86D594A3F0D2B28A7DDB152D7A0D8DB8EB9863AA383AC51FAE28_1563750307356_imagen.png)
    - ![](https://paper-attachments.dropbox.com/s_F4E2FA96254E86D594A3F0D2B28A7DDB152D7A0D8DB8EB9863AA383AC51FAE28_1563750427239_imagen.png)