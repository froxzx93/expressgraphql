module.exports = {

    development: {
        port: 3000,
        message: "Server of development is running in port"
    },

    production: {
        port: 3000,
        message: "Server of production is running in port"
    }

}