# CRUD GRAPHQL

## **Get market**
- **Query**
    query queryMarket($id: String!) {
      getMarket(id: $id) {
        categoria
        nombre
        id
        direccion
      }
    }
- **Query variable**
    {
      "id":"2X9OuN7MK7sUxpfQ8BM5"
    }
- **Response**
    {
      "data": {
        "getMarket": {
          "categoria": "Tecnology",
          "nombre": "Store999",
          "id": "2X9OuN7MK7sUxpfQ8BM5",
          "direccion": "Alameda 02220"
        }
      }
    }
- **Capture**
![](https://paper-attachments.dropbox.com/s_F4E2FA96254E86D594A3F0D2B28A7DDB152D7A0D8DB8EB9863AA383AC51FAE28_1563755383025_imagen.png)

## **Get markets**
- **Query**
    {
      getMarkets{
        nombre
        direccion
        id
        categoria
      }
    }
- **Response**
    {
        "data": {
            "getMarket": [
                {
                    "nombre": "Store999",
                    "direccion": "Alameda 02220",
                    "id": "s9W78HqffEiYzrD5HaPG",
                    "categoria": "Tecnology"
                },
                {
                    "nombre": "Store999",
                    "direccion": "Alameda 02220",
                    "id": "tV4OArTfZE71bkFNdeLW",
                    "categoria": "Tecnology"
                },
                {
                    "nombre": "pollo super",
                    "direccion": "zona 3",
                    "id": "txwTI5nIPRxE5puuMEDX",
                    "categoria": "carniceria"
                },
                {
                    "nombre": "Store999",
                    "direccion": "Alameda 02220",
                    "id": "xUXWciIKvjPDMKEq8Sav",
                    "categoria": "Tecnology"
                },
                {
                    "nombre": "Store999",
                    "direccion": "Alameda 02220",
                    "id": "yTzxCw5wkYo8FKuveWSo",
                }
            ]
        }
    }
- **Capture**
![](https://paper-attachments.dropbox.com/s_F4E2FA96254E86D594A3F0D2B28A7DDB152D7A0D8DB8EB9863AA383AC51FAE28_1563755622735_imagen.png)

## **Add market**
- **Query**
    mutation {
      pushMarket(input: {
        categoria: "Tecnology"
        nombre: "Store999"
        direccion: "Alameda 02220"
      }){
        categoria
        direccion
        id
        nombre
      }
    }
- **Response**
    {
      "data": {
        "pushMarket": {
          "categoria": "Tecnology",
          "direccion": "Alameda 02220",
          "id": "2X9OuN7MK7sUxpfQ8BM5",
          "nombre": "Store999"
        }
      }
    }
- Capture
![](https://paper-attachments.dropbox.com/s_F4E2FA96254E86D594A3F0D2B28A7DDB152D7A0D8DB8EB9863AA383AC51FAE28_1563755353778_imagen.png)

## **Update market**
- **Query**
    mutation {
      setMarket(input: {
        id:"2X9OuN7MK7sUxpfQ8BM5"
        categoria: "Panaderia"
        nombre: "El marraqueta 11"
        direccion: "Alameda 9999"
      }){
        categoria
        direccion
        id
        nombre
      }
    }
- **Response**
    {
      "data": {
        "setMarket": {
          "categoria": "Panaderia",
          "direccion": "Alameda 9999",
          "id": "2X9OuN7MK7sUxpfQ8BM5",
          "nombre": "El marraqueta 11"
        }
      }
    }
- Capture
![](https://paper-attachments.dropbox.com/s_F4E2FA96254E86D594A3F0D2B28A7DDB152D7A0D8DB8EB9863AA383AC51FAE28_1563756258249_imagen.png)

## D**elete market**
- **Query**
    mutation {
      deleteMarket(input: {
        id:"2X9OuN7MK7sUxpfQ8BM5"
      }){
        text
        status
      }
    }
- **Response**
    {
      "data": {
        "deleteMarket": {
          "text": "Delete successfully2X9OuN7MK7sUxpfQ8BM5",
          "status": 200
        }
      }
    }
- Capture
![](https://paper-attachments.dropbox.com/s_F4E2FA96254E86D594A3F0D2B28A7DDB152D7A0D8DB8EB9863AA383AC51FAE28_1563758852304_imagen.png)

